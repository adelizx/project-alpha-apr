from django.urls import path
from projects.views import proj_listview, proj_detailview, create_project

urlpatterns = [
    path("", proj_listview, name="list_projects"),
    path("<int:id>/", proj_detailview, name="show_project"),
    path("create/", create_project, name="create_project"),
]
