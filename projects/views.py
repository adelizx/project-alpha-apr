from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


# Feature 5 - List View
@login_required
def proj_listview(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)


# Feature 13 - Detail View
@login_required
def proj_detailview(request, id):
    proj_details = Project.objects.get(id=id)
    context = {"proj_details": proj_details}
    return render(request, "projects/details.html", context)


# Feature 14 - Create Project View
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
