from django.forms import ModelForm
from projects.models import Project


# Feature 14 - Create Project View
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
